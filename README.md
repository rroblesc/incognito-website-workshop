# Static websites on GitLab using Hugo

## Introduction

**Why use GitLab over GitHub for static pages?**

I find it's a bit more flexible and only a _bit_ more difficult (you'll need to use the terminal once here). Also, GitLab provides easy ways to deploy and configure static pages using different static-site generator engines. Also it's not Microsoft. 

**Why Hugo?**

To be honest, no particular reason other than I liked the GitLab repository template layout, found Hugo quite intuitive, and I used it for my Natural Language Processing course assignment [website](https://p-skaisgiris.gitlab.io/ina-website/).

## Useful resources

- https://gitlab.com/pages/hugo
- https://gohugo.io/hosting-and-deployment/hosting-on-gitlab/
- https://docs.gitlab.com/ee/ci/git_submodules.html#using-git-submodules-in-your-ci-jobs <-- Useful for setting up themes for Hugo
- https://github.com/rhazdon/hugo-theme-hello-friend-ng <-- Documentation for theme we will apply today

## Preliminary steps

1. Create a GitLab account.
2. Create a new GitLab project using GitLab pages Hugo template by pressing on "New project" -> "Create from template" -> Pages/Hugo "Use template"
3. On the sidebar, navigate to "CI/CD" -> "Pipelines" -> "Run Pipeline" -> "Run Pipeline". This will trigger the website to be deployed on GitLab. 
4. And you have a default Hugo website running on <username>.gitlab.io/<project-name>! It's that easy!

## Customization

In your website repository `config.toml` is an important file. It is basically the configuration file for your static website, you define the themes and layout of your pages.

First, let's do some initial clean-up. We need to modify `baseurl` to point to your repository and not the general template. So, modify `baseurl` as follows:
```
baseurl = "https://<username>.gitlab.io/<project-name/>"
```

So, for this particular example it's:


```
baseurl = "https://p-skaisgiris.gitlab.io/static-website-workshop-hugo/"
```

For in-depth documentation for the configuration, visit [here](https://gohugo.io/getting-started/configuration/). Furthermore, usually more configuration info can be found on each theme's documentation.


### Changing website title

To change the Title of the default page, simply change `title` in `config.toml`

### Changing menu

The following code at the bottom of `config.toml`

```
[[menu.main]]
  parent = "About"
  url = "page/about/"
  weight = 3
```

adds an "About" tab in the main menu of the website. `url` points to a specific page that will be opened if "About" is pressed. Notice that the path starts in the `content` directory. If you refer to a directory in `url`, it will appear as a list of posts/pages, for example, like

```
[[menu.main]]
  name = "Blog"
  url = ""
  weight = 1
```

Since `url` is `""`, it means the "Blog" tab points to all contents in the `content` directory and thus lists previews of  all Markdown (.md) files.

### Changing post content / writing new posts

Take a look at some sample posts, e.g. `content/post/2017-03-05-math-sample.md`.

Notice the syntax at the top of the Markdown file:

```
---
title: Math Sample
subtitle: Using KaTeX
date: 2017-03-05
tags: ["example", "math"]
---
```

This (quite intuitively) defines the title, subtitle, date, and tags of the post. Then you can simply channel your inner creativity and write whatever you wish! All the usual Markdown syntax bits apply. For a guide on Markdown, visit [here](https://www.markdownguide.org/).

### Changing the theme

Probably the most important bit, isn't it?

You can find the ready-made themes for Hugo [here](https://themes.gohugo.io/). As you can see, there is a _wide_ variety of themes ranging from resume-only to docs to blogs to company websites.

I will show you how to setup the slick (dark!) [hello-friend-ng](https://themes.gohugo.io/hugo-theme-hello-friend-ng/) theme.

This is the part where you will have to use the command line. In case you are using Windows, refer to [this video](https://www.youtube.com/watch?v=zy8epgWe-no).

So, the spooky part. Make sure you have `git` installed and configured. The steps:

1. Clone your repository using `git clone`. In our example, it's `git clone https://gitlab.com/p-skaisgiris/incognito-website-workshop.git`
2. Navigate to the cloned repository in the terminal using `cd`
3. Add the 'hello-friend-ng' theme submodule using `git submodule add https://github.com/rhazdon/hugo-theme-hello-friend-ng.git themes/hello-friend-ng`. This will add the theme dependency in your repository and place it in the `themes` directory. You can easily alter this step to incorporate some other Hugo theme.
4. Overwrite the `config.toml` from [here](https://themes.gohugo.io/hugo-theme-hello-friend-ng/#how-to-configure) into your `config.toml` 
5. Some minor **yet important** changes in `config.toml`. Change the `baseurl` to your website name (e.g. `baseurl = "https://p-skaisgiris.gitlab.io/incognito-website-workshop/"`). Change the "Blog" tab `url` to `url = "post/"`. Delete `content/post/2017-03-20-photoswipe-gallery-sample.md` as there's some bit missing and it breaks the pipeline.
6. For this specific theme, we need Hugo Extended and thus change the `image` in `.gitlab-ci.yml` to:
```
image: registry.gitlab.com/pages/hugo/hugo_extended:latest
```

7. Add, commit changes and push to the repository using something like
`git add .` 
`git commit -m "Add hello-friend-ng theme"`
`git push`

### Troubleshooting

Usually if something is wrong, you will be notified that the "pipeline failed". You can inspect the error by going to "CI/CD" -> "Pipelines" and pressing on the failed pipeline.


